﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardDav
{
    public class vCardCollection : IEnumerable<iCloudvCard>
    {
        public vCardCollection(CardDavClient client)
        {
            this.Client = client;
        }

        public CardDavClient Client { get; set; }
        public IEnumerable<iCloudvCard> vCards { get; set; }

        public iCloudvCard this[string vCardUrl]
        {
            get
            {
                return CardDavUtils.GetvCardSingle(Client, vCardUrl);
            }
        }

        private void Refresh()
        {
            List<string> vCardUrls = CardDavUtils.GetvCardUrls(Client);
            vCards = CardDavUtils.GetvCards(Client, vCardUrls).Where(c => !c.IsGroupvCard);
        }

        public IEnumerator<iCloudvCard> GetEnumerator()
        {
            Refresh();
            return vCards.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            Refresh();
            return vCards.GetEnumerator();
        }
    }
}
