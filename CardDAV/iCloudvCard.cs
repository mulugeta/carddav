﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Thought.vCards;

namespace CardDav
{
    public class iCloudvCard : vCard
    {
        public iCloudvCard()
        {
        }

        public iCloudvCard(string content, string url)
            : this(new StringReader(content), url)
        {
            this.Url = url;
        }

        public iCloudvCard(TextReader input, string url)
            : base()
        {
            GroupMembers = new List<string>();

            iCloudvCardReader reader = new iCloudvCardReader();
            reader.ReadInto(this, input);
        }

        public bool IsGroupvCard { get; set; }
        public string Url { get; set; }
        public List<string> GroupMembers { get; private set; }
    }
}
