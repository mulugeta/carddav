﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardDav
{
    public class CardDavClient
    {
        public CardDavClient(string serverUrl, CardDAVAccount account)
        {
            RootServer = serverUrl;
            this.Account = account;

            // iCloud has only one collection/folder
            DefaultCollection = CardDavUtils.GetContactCollections(this).Single();
            vCards = new vCardCollection(this);
        }

        public CardDavClient(string serverUrl, string appleId, string password)
            : this(serverUrl, new CardDAVAccount(appleId, password))
        {
        }

        private string addressbookUrl;

        public CardDAVAccount Account { get; set; }
        public string RootServer {get;set;}

        public string AddressbookURL
        {
            get
            {
                if (addressbookUrl == null)
                {
                    addressbookUrl = CardDavUtils.GetAddressbookURL(RootServer, this);
                }

                return addressbookUrl;
            }
        }

        public IEnumerable<ContactGroup> Groups
        {
            get
            {
                return CardDavUtils.GetGroups(this);
            }
        }

        public vCardCollection vCards { get; set; }

        public VCardData CreatevCard(iCloudvCard vc)
        {
            return CardDavUtils.CreatevCard(this, vc);
        }

        public VCardData UpdatevCard(iCloudvCard vc)
        {
            return CardDavUtils.UpdatevCard(this, vc);
        }

        public void DeletevCard(iCloudvCard vc)
        {
            CardDavUtils.DeletevCard(this, vc.Url);
        }

        public ContactCollection DefaultCollection { get; set; }
    }
}
