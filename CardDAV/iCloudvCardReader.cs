﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Thought.vCards;

namespace CardDav
{
    public class iCloudvCardReader : vCardStandardReader
    {
        public override void ReadInto(vCard card, TextReader reader)
        {
            vCardProperty property;

            do
            {
                property = ReadProperty(reader);

                if (property != null)
                {

                    if (
                        (string.Compare("END", property.Name, StringComparison.OrdinalIgnoreCase) == 0) &&
                        (string.Compare("VCARD", property.ToString(), StringComparison.OrdinalIgnoreCase) == 0))
                    {

                        // This is a special type of property that marks
                        // the last property of the vCard. 

                        break;
                    }
                    else
                    {
                        ReadIntoiCloudvCard((iCloudvCard)card, property);
                    }
                }

            } while (property != null);

        }

        public void ReadIntoiCloudvCard(iCloudvCard card, vCardProperty property)
        {
            base.ReadInto(card, property);

            switch (property.Name.ToUpperInvariant())
            {

                case "X-ADDRESSBOOKSERVER-KIND":
                    if (property.Value.ToString().ToLower() == "group")
                        card.IsGroupvCard = true;
                    break;

                case "X-ADDRESSBOOKSERVER-MEMBER":
                    string val = property.Value.ToString();
                    card.GroupMembers.Add(val.Substring(val.LastIndexOf(":") + 1));
                    break;

                default:

                    // The property name is not recognized and
                    // will be ignored.

                    break;

            }
        }
    }
}
