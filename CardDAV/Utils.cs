﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Thought.vCards;

namespace CardDav
{
    public class CardDavUtils
    {
        public static string Version = "1.0";
        public static string UserAgent = "CardDavClient.NET";

        public static XNamespace dav = "DAV:";
        public static XNamespace cardDav = "urn:ietf:params:xml:ns:carddav";

        public static string GetAddressbookURL(string rootServer, CardDavClient client)
        {
            string principalQuery = @"<?xml version=“1.0“ encoding=“utf-8“?>
<D:propfind xmlns:D=“DAV:“>
	<D:prop>
		<D:current-user-principal />
		<D:resourcetype />
	</D:prop>
</D:propfind>";

            principalQuery = principalQuery.Replace("“", "\"");

            Dictionary<string, string> prinRes = CardDavUtils.Query(client, rootServer, "PROPFIND", principalQuery, @"text/xml");
            string href = XDocument.Parse(CardDavUtils.GetRaw(prinRes)).Descendants(dav + "current-user-principal")
                .Elements(dav + "href").First().Value;

            string addressBookQuery0 = @"<?xml version=“1.0“ encoding=“utf-8“?><D:propfind xmlns:D=“DAV:“>
	<D:prop><D:displayname />
		<schedule-inbox-URL xmlns=“urn:ietf:params:xml:ns:caldav“ />
		<schedule-outbox-URL xmlns=“urn:ietf:params:xml:ns:caldav“ />
		<calendar-user-address-set xmlns=“urn:ietf:params:xml:ns:caldav“ />
		<default-calendar-URL xmlns=“http://icewarp.com/ns/“ />
		<default-tasks-URL xmlns=“http://icewarp.com/ns/“ />
		<default-contacts-URL xmlns=“http://icewarp.com/ns/“ />
		<calendar-home-set xmlns=“urn:ietf:params:xml:ns:caldav“ />
		<addressbook-home-set xmlns=“urn:ietf:params:xml:ns:carddav“ />
	</D:prop>
</D:propfind>";
            addressBookQuery0 = addressBookQuery0.Replace("“", "\"");

            Dictionary<string, string> abRes = CardDavUtils.Query(client, rootServer + href, "PROPFIND", addressBookQuery0, @"text/xml");
            string addressBookUrl = XDocument.Parse(CardDavUtils.GetRaw(abRes)).Descendants(cardDav + "addressbook-home-set")
                .Elements(dav + "href").First().Value;
            return addressBookUrl;
        }

        public static List<string> GetvCardUrls(CardDavClient client)
        {
            string allQuery = @"<?xml version=“1.0“ encoding=“utf-8“?>
                    <D:sync-collection xmlns:D=“DAV:“>
	                    <D:prop>
		                    <D:getetag />
		                    <D:getcontenttype />
	                    </D:prop>
                    </D:sync-collection>";

            allQuery = allQuery.Replace("“", "\"");

            Dictionary<string, string> result = CardDavUtils.Query(client, client.RootServer + client.DefaultCollection.Url, "REPORT", allQuery, @"text/xml");

            string xml = CardDavUtils.GetRaw(result);
            XDocument xdoc = XDocument.Parse(xml);

            List<string> vCardUrls = xdoc.Element(dav + "multistatus").Elements(dav + "response").Elements(dav + "href").Where(d => d.Value.EndsWith(".vcf")
                && d.Parent.Elements(dav + "status").Count() == 0
                && d.Parent.Descendants(dav + "status").Select(x => new { Val = x.Value.ToLower() })
                    .Where(x => x.Val.Contains("ok")
                            || x.Val.Contains("200")).SingleOrDefault() != null).Select(d => d.Value).ToList();

            return vCardUrls;
        }

        public static IEnumerable<ContactGroup> GetGroups(CardDavClient client)
        {
            var groupvCards = GetvCards(client, GetvCardUrls(client)).Where(vc => vc.IsGroupvCard);
            return groupvCards.Select(vc => new ContactGroup(vc.FamilyName, vc.GroupMembers, client));
        }

        public static IEnumerable<iCloudvCard> GetvCards(CardDavClient client, List<string> vCardUrls)
        {
            IEnumerable<VCardData> vCardData = GetvCardData(client, vCardUrls);
            return vCardData.Select(vcd => new iCloudvCard(vcd.Content, vcd.Url));
        }

        public static IEnumerable<VCardData> GetvCardData(CardDavClient client, List<string> vCardUrls)
        {
            var vCardXmls = vCardUrls.WrapInXmlNodes();

            string vcardsQuery = @"<?xml version=“1.0“ encoding=“utf-8“?>
<C:addressbook-multiget xmlns:C=“urn:ietf:params:xml:ns:carddav“>
	<D:prop xmlns:D=“DAV:“>
		<D:getetag />
		<C:address-data />
	</D:prop>".Replace("“", "\"")

            + String.Join(Environment.NewLine, vCardXmls)

+ "</C:addressbook-multiget>";

            Dictionary<string, string> result2 = CardDavUtils.Query(client, client.AddressbookURL, "REPORT", vcardsQuery, @"text/xml");

            string xml2 = CardDavUtils.GetRaw(result2);

            XDocument xdoc2 = XDocument.Parse(xml2);
            return xdoc2.Descendants(cardDav + "address-data")
                .Select(d => new VCardData
                {
                    Url = d.Ancestors(dav + "response").Elements(dav + "href").First().Value,
                    Content = d.Value,
                    ETag = d.Ancestors(dav + "prop").Elements(dav + "getetag").First().Value
                });
        }

        public static VCardData CreatevCard(CardDavClient client, iCloudvCard vcard)
        {
            if (String.IsNullOrWhiteSpace(vcard.UniqueId))
                vcard.UniqueId = Guid.NewGuid().ToString();

            string vcText = GetVCardAsText(vcard);

            Uri uri = new Uri(client.AddressbookURL);
            string serverURL = uri.Scheme + "://" + uri.Host;
            string uriPart = client.AddressbookURL.Substring(client.AddressbookURL.IndexOf('/', serverURL.Length));

            // Save to first folder by default
            vcard.Url = uriPart + client.DefaultCollection.Name + "/" + Guid.NewGuid() + ".vcf";

            string newETag = SavevCard(client, new VCardData(vcText, vcard.Url, null), false, true);

            return new VCardData(vcText, vcard.Url, newETag);
        }

        public static VCardData UpdatevCard(CardDavClient client, iCloudvCard vcard)
        {
            string vcText = GetVCardAsText(vcard);
            string newETag = SavevCard(client, new VCardData(vcText, vcard.Url, null), false, true); // same url

            return new VCardData(vcText, vcard.Url, newETag);
        }

        public static void DeletevCard(CardDavClient client, string vCardUrl)
        {
            Uri uri = new Uri(client.AddressbookURL);
            string serverURL = uri.Scheme + "://" + uri.Host;

            Dictionary<string, string> putRes = Query(client, serverURL + vCardUrl, "DELETE");
        }

        public static string GetVCardAsText(vCard vc)
        {
            string vcString;

            using (var ms = new MemoryStream())
            {
                var sw = new StreamWriter(ms);
                var vcardw = new vCardStandardWriter();

                var mi = typeof(vCardStandardWriter).GetMethod("BuildProperties", BindingFlags.NonPublic | BindingFlags.Instance);
                vCardPropertyCollection properties = (vCardPropertyCollection)mi.Invoke(vcardw, new object[] { vc });
                properties.Insert(1, new vCardProperty("VERSION", "3.0"));
                vcardw.Write(properties, sw);

                sw.Flush();
                ms.Position = 0;

                using (var streamReader = new StreamReader(ms))
                {
                    vcString = streamReader.ReadToEnd();
                }
            }

            return vcString;
        }

        public static string SavevCard(CardDavClient client, VCardData vCardData, bool includeChangeCheck, bool isNew)
        {
            Dictionary<string, object> headers = new Dictionary<string, object>();
            if (includeChangeCheck)
            {
                headers.Add("If-Match", vCardData.ETag);
            }

            Uri uri = new Uri(client.AddressbookURL);
            string serverURL = uri.Scheme + "://" + uri.Host;

            Dictionary<string, string> putRes = Query(client, serverURL + vCardData.Url, "PUT", vCardData.Content, "text/vcard", headers);

            // TODO lock resource here or get it from server with PUT? to avoid its getting modified in the meantime
            var card = GetvCardDataSingle(client, vCardData.Url);

            return card.ETag; // always changes after an update (even if not new entry)
        }

        public static iCloudvCard GetvCardSingle(CardDavClient client, string vCardUrl)
        {
            VCardData vcd = GetvCardDataSingle(client, vCardUrl);
            if (vcd == null)
                return null;
            return new iCloudvCard(vcd.Content, vcd.Url);
        }

        public static VCardData GetvCardDataSingle(CardDavClient client, string vCardUrl)
        {
            List<string> vCardUrls = new List<string> { vCardUrl };
            return GetvCardData(client, vCardUrls).SingleOrDefault();
        }

        public static Dictionary<string, string> Query(CardDavClient client, string url, string method, string content = null, string contentType = null,
             Dictionary<string, object> headers = null)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            if (headers != null && headers.Count > 0)
            {
                foreach (var pair in headers)
                {
                    request.Headers.Add(pair.Key, pair.Value.ToString());
                }
            }

            Dictionary<string, string> queryResponse = new Dictionary<string, string>();

            request.UserAgent = UserAgent + "/" + Version;
            request.Method = method;
            //request.PreAuthenticate = true;

            string _auth = string.Format("{0}:{1}", client.Account.AppleId, client.Account.Password);
            string _enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));
            string _cred = string.Format("{0} {1}", "Basic", _enc);
            request.Headers[HttpRequestHeader.Authorization] = _cred;
            //request.Headers.Add("Authorization", _cred);

            ////request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(this.authentication)));
            //request.Credentials = new NetworkCredential(username, password);

            //WebProxy proxy = new WebProxy();
            //proxy.Address = new Uri("http://proxy:80");
            //request.Proxy = proxy;

            if (contentType != null)
            {
                request.ContentType = contentType;
            }

            if (content != null)
            {
                byte[] contentBytes = Encoding.UTF8.GetBytes(content);
                request.ContentLength = contentBytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(contentBytes, 0, contentBytes.Length);
                requestStream.Close();
            }
            else
                request.ContentLength = 0;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                queryResponse.Add("status", response.StatusCode.ToString());

                using (Stream responseStream = response.GetResponseStream())
                {
                    byte[] buffer = new byte[2048];

                    StringBuilder responseBuilder = new StringBuilder();

                    int amountRead = 0;

                    amountRead = responseStream.Read(buffer, 0, 2048);

                    while (amountRead > 0)
                    {
                        string tempString = Encoding.UTF8.GetString(buffer, 0, amountRead);
                        responseBuilder.Append(tempString);
                        amountRead = responseStream.Read(buffer, 0, 2048);
                    }

                    queryResponse.Add("response", responseBuilder.ToString());
                }
            }

            return queryResponse;
        }

        public static IEnumerable<ContactCollection> GetContactCollections(CardDavClient client)
        {
            string addressBookQuery = @"<?xml version=“1.0“ encoding=“utf-8“?>
<D:propfind xmlns:D=“DAV:“>
	<D:prop>
		<D:getetag />
		<D:displayname />
		<D:resourcetype />
		<supported-calendar-component-set xmlns=“urn:ietf:params:xml:ns:caldav“ />
		<supported-address-data xmlns=“urn:ietf:params:xml:ns:carddav“ />
		<xmpp-uri xmlns=“http://calendarserver.org/ns/“ />
		<calendar-color xmlns=“http://apple.com/ns/ical/“ />
		<D:supported-report-set />
	</D:prop>
</D:propfind>";
            addressBookQuery = addressBookQuery.Replace("“", "\"");

            Dictionary<string, object> headers = new Dictionary<string, object>();
            headers.Add("Depth", 1);

            Dictionary<string, string> foldersRes = CardDavUtils.Query(client, client.AddressbookURL, "PROPFIND", addressBookQuery, @"text/xml", headers);

            string[] folders = XDocument.Parse(CardDavUtils.GetRaw(foldersRes)).Descendants(dav + "response")
                .Where(r => r.Descendants(dav + "resourcetype").Count() > 0
                    && r.Descendants(dav + "resourcetype").Elements(cardDav + "addressbook").Count() > 0).Elements(dav + "href").Select(el => el.Value).ToArray();

            return folders.Select(name => new ContactCollection(name, client));
        }

        public static string GetRaw(Dictionary<string, string> result)
        {
            if (result.ContainsKey("status"))
            {
                string status = result["status"];

                if (status.Equals("200") || status.Equals("207"))
                {
                    if (result.ContainsKey("response"))
                    {
                        return result["response"];
                    }
                    else
                    {
                        throw new CardDavException("Arrgg. No response returned from the server!");
                    }
                }
                else
                {
                    throw new CardDavException("Whoops something went wrong! The server returned a status code of: " + status);
                }
            }
            else
            {
                throw new CardDavException("No status code returned from HTTP Request");
            }
        }
    }

    public class CardDavException : Exception
    {
        public CardDavException()
            : base()
        {

        }

        public CardDavException(string message)
            : base(message)
        {
        }
    }
}
