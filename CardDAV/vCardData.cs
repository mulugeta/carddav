﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardDav
{
    public class VCardData
    {
        public VCardData()
        {
        }

        public VCardData(string content, string url, string etag)
        {
            this.Content = content;
            this.Url = url;
            this.ETag = etag;
        }

        public string Url { get; set; }
        public string Content { get; set; }
        public string ETag { get; set; }
    }
}
