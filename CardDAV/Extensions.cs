﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardDav
{
    public static class Extensions
    {
        public static List<string> WrapInXmlNodes(this List<string> urls)
        {
            var nodes = urls.Select(d => "<D:href xmlns:D=\"DAV:\">" + d + "</D:href>").ToList();
            return nodes;
        }
    }
}
