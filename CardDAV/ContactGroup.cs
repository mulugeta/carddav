﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thought.vCards;

namespace CardDav
{
    public class ContactGroup
    {
        public ContactGroup(string name, List<string> vCardUUIDs, CardDavClient client)
        {
            this.Name = name;
            this.vCardUUIDs = vCardUUIDs;
            this.Client = client;
        }

        public string Name { get; set; }
        public List<string> vCardUUIDs { get; set; }
        public CardDavClient Client { get; set; }

        public IEnumerable<vCard> vCards
        {
            get
            {
                List<string> vCardUrls = vCardUUIDs.Select(u => Client.AddressbookURL + "card/" + u + ".vcf").ToList();
                return CardDavUtils.GetvCards(Client, vCardUrls);
            }
        }
    }
}
