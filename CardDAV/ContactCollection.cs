﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardDav
{
    public class ContactCollection
    {
        public ContactCollection(string url, CardDavClient client)
        {
            this.Url = url;
            this.Client = client;
        }

        public string Url { get; set; }
        public string Name
        {
            get
            {
                return this.Url.Trim('/').Split('/').Last();
            }
        }
        public CardDavClient Client { get; set; }
    }
}
