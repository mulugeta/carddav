﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardDav
{
    public class CardDAVAccount
    {
        public CardDAVAccount(string appleId, string password)
        {
            this.AppleId = appleId;
            this.Password = password;
        }

        public string AppleId { get; set; }
        public string Password { get; set; }
    }
}
