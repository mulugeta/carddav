﻿Imports CardDav
Imports Thought.vCards
Imports System.Configuration

Module Test1

    Sub Main()

        ' First get these
        '
        Dim appleId As String = ConfigurationManager.AppSettings("username")
        Dim password As String = ConfigurationManager.AppSettings("password")

        Dim client As CardDavClient = New CardDavClient("https://contacts.icloud.com/", appleId, password)

        Console.WriteLine(client.Groups.First().Name)
        Dim samplevCardUrl
        For Each vCard As iCloudvCard In client.Groups.First().vCards
            Console.WriteLine(vCard.FamilyName)
            samplevCardUrl = vCard.Url
        Next

        ' All contacts
        Console.WriteLine("All Contacts:")
        For Each vCard As vCard In client.vCards
            Console.WriteLine(vCard.FamilyName)
        Next

        ' Get specific vCard - you need to supply the vcard url
        ' The id in the vCard url may not be the same as unique id of the vCard
        Console.WriteLine("Specific Contact:")
        Dim vCard2 As iCloudvCard = client.vCards(samplevCardUrl)
        Console.WriteLine(vCard2.FamilyName)


        ' Create =====================================================================================
        ' GivenName  and FormattedName re required
        '
        Dim card1 As New iCloudvCard
        With card1
            .GivenName = "Test Contact " + DateTime.Now ' Required 
            .FormattedName = "The Test Contact " + DateTime.Now ' Required
            .Phones.Add(New vCardPhone("28322920", vCardPhoneTypes.Cellular))
            ' ...
        End With

        Dim data1 As New VCardData

        data1 = client.CreateVCard(card1)

        ' Update =====================================================================================
        Dim card2 As iCloudvCard = client.vCards(card1.Url)
        card2.GivenName = card2.GivenName + "Updated" ' make some change
        client.UpdatevCard(card2)

        ' Delete ======================================================================================
        client.DeletevCard(card1)

        Console.ReadKey()

    End Sub

End Module
